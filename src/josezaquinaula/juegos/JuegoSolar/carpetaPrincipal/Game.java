package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.GameCamera;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.keyManager.MouseManager;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.States.GameState;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.States.MenuState;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.States.SettingState;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.States.State;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Assets;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.keyManager.KeyManager;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class Game implements Runnable{
    //todo el juego
    private Display Display; //make the window
    public String title;
    private int  width,lenght;
    private BufferStrategy bs; //hidden screen
    private Graphics g; //hacer formas
    private Thread thread; //threads .-.
    public boolean running=false; //variable who start the while
    private int i=0; //temporal
    public State gameState,menuState,settingState;
    //game camera
    private GameCamera gameCamera;
    //handler
    private Handler handler;
    //input
    private KeyManager keyManager;
    private MouseManager mouseManager;

    public Game(String title,int width,int lenght) {
        this.width=width;
        this.lenght=lenght;
        this.title=title;   //pone el valor de los valores ingresados en el contructor en las de esta clase
        keyManager=new KeyManager();
        mouseManager=new MouseManager();
    }

    public void init(){
        Display =new Display(title,width,lenght);
        //input
        Display.addKeyListener(keyManager);
        Display.addMouseListener(mouseManager);
        Display.addMouseMotionListener(mouseManager);
        Display.getCanvas().addMouseListener(mouseManager);
        Display.getCanvas().addMouseMotionListener(mouseManager);
        //inicializolas texturas del juego
        Assets.init();
        //gfx
        handler=new Handler(this);
        gameCamera=new GameCamera(handler,0,0);

        //inicializo el State que quiero (CUAL VENTANA DEL JUEGO)
        gameState=new GameState(handler);
        menuState=new MenuState(handler);
        settingState=new SettingState(handler);
        State.setState(gameState); //TEMPORAL, principal es menustate cuando se trabaje con State, se trbajara con la ventana gameState
    }

    @Override
    public void run() {
        init();
        //configuro variables para las FPS del juego
        int fps=60;
        double timePerTicks=1000000000/fps; //importante double por ser valores pequeños
        long now; //al ser valores simplificados es mas eficiente con tipo long
        long lastTime=System.nanoTime();
        int ticks=0;
        double delta=0; //valoresmuy pequeños
        long timer=0;
        //el bucle del juego
        while(running) {
            //configuro FPS del juego
            now = System.nanoTime();
            delta += ((now - lastTime) / timePerTicks);
            timer += now - lastTime;
            lastTime = now;

            if (delta >= 1) {
                tick();
                render();
                ticks++;
                delta--;
            }
            //if(timer>= 1000000000){
                //System.out.println("Time and frame: "+ticks+"delta: "+delta);
                //ticks=0;
                //timer=0;
        //    }
        }
        stop();
    }

    private void render() { //para dibujar
        //configurar state.render

        //configurar graficos
        bs = Display.getCanvas().getBufferStrategy();
        if(bs==null){
            Display.getCanvas().createBufferStrategy(3); //3buffers (3 hidden screens)
            return;
        }
        g=bs.getDrawGraphics(); //porque es igual a un getter de DrawGraphics de un bs??(creo que es igual a un grafico hecho en el buffer
        g.clearRect(0,0,width,lenght); //crea un rectangulo gigante que borra todo a su paso
        //start paint¡
        if (State.getState()!=null){
         State.getState().render(g);
        }
        //stop paint¡
        bs.show(); //mostar el ultimo buffer
        g.dispose(); //verifica si el grafico esta hecho bien
    }

    private void tick() { //para actualizar posicion de objetos y etc
        //configurar state.tick
        if (State.getState()!=null){
            State.getState().tick();
        }
        //
        keyManager.tick();
    }

    public synchronized void start(){
        if (running)
            return; //si ya esta corriendo no lo vuelvas a correr
        thread=new Thread(this);
        thread.start();
        running=true;

    }
    public synchronized void stop(){
        if (!running)
            return; //si esta parado no lo vuelvas a parar
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
    //getters and setters
    public int getWidth(){return width;}
    public int getLenght(){return lenght;}
    public GameCamera getGameCamera(){ return gameCamera;}
    public KeyManager getKeyManager() {
        return keyManager;
    }

    public MouseManager getMouseManager() {
        return mouseManager;
    }
}
