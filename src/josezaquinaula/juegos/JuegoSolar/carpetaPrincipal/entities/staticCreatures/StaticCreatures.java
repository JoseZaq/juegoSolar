package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.staticCreatures;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.Entity;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;

public abstract class StaticCreatures extends Entity {
    public StaticCreatures(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
    }

}
