package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.staticCreatures;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Assets;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.items.Item;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.tile.Tile;

import java.awt.*;

public class Tree extends StaticCreatures{
    public Tree(Handler handler, float x, float y) {
        super(handler, x, y, Tile.TILEWIDTH,Tile.TILEHEIGTH*2);
        bounds.x=20;
        bounds.y=108;
        bounds.width=20;
        bounds.height=20;
    }

    @Override
    public void die() {
        //el item que se ve despues de morir
        handler.getWorld().getItemManager().addItem(Item.woodItem.createItem((int)(x),(int)(y)));
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.tree,(int)(x-handler.getGameCamera().getxOffset()),
                (int)(y-handler.getGameCamera().getyOffset()),width,height,null);

    }

}
