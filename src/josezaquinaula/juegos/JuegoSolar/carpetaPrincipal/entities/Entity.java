package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;

import java.awt.*;

public abstract class Entity {
    protected static final int DEFAULT_HEALTH=3;
    //variables
    protected int width,height;
    protected Handler handler;
    protected int health;
    //existe o no existe
    protected boolean active=true;
    //rectangulo de colisiones
    protected Rectangle bounds;
    //entidades del juego
    protected float x,y;
    public Entity(Handler handler, float x, float y, int width, int height){
        this.x=x;
        this.y=y;
        this.height=height;
        this.width=width;
        this.handler =handler;

        health=DEFAULT_HEALTH;
        bounds=new Rectangle(0,0,width,height);
    }
    public boolean checkEntityCollision(float xOffset,float yOffset){
        //compara posiciones de todas las entidades con esta y manda true si es que choka
        for(Entity e:handler.getWorld().getEntityManager().getEntities()){
            if(e.equals(this)) continue;
            if(e.getCollisionBounds(0f,0f).intersects(getCollisionBounds(xOffset,yOffset))){
                return true;
            }
        }
        return false;
    }
    public Rectangle getCollisionBounds(float xOffset,float yOffset){
        //devuelve un rectangulo en la posicion del bound de la entidad aumentado un offset
        //(para verificar si el siguiente paso va a colisionar con otro bound
        return new Rectangle((int)(x+bounds.x+xOffset),(int)(y+ bounds.y+yOffset),bounds.width,bounds.height);
    }

    public void hurt(int amt){
        health-=amt;
        if(health<=0){
            active=false;
            die();
        }
    }

    public abstract void die();
    public abstract void tick();
    public abstract void render(Graphics g);

    //getters and setters


    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
