package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.creatures;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.Entity;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.tile.Tile;

public abstract class Creature extends Entity {
    //variables
    protected static final float DEFAULT_SPEED=3.0f;
    protected static final int DEFAULT_WIDTH=64,
                               DEFAULT_HEIGHT=64;
    protected float speed;

    protected float xmove,ymove;

    public Creature(Handler handler, float x, float y, int width, int height) {
        super(handler,x, y,width,height);
        speed=DEFAULT_SPEED;
        xmove=0;
        ymove=0;
    }
    public void move(){
        if(!checkEntityCollision(xmove,0f))
        Xmove();
        if(!checkEntityCollision(0f,ymove))
        Ymove();
    }
    public void Xmove(){
        if(xmove>0) {//movimiento a la derecha
            int tx = (int) (x + xmove + bounds.x + bounds.width) / Tile.TILEWIDTH;
            //si el tile donde esta el bound es solido muevete
            if (!collisionWithTile(tx, (int) (y + bounds.y) / Tile.TILEHEIGTH)
                    && !collisionWithTile(tx, (int) (y + bounds.y + bounds.height) / Tile.TILEHEIGTH)) {
                x += xmove;
            }else{
                x=tx* Tile.TILEWIDTH-bounds.x- bounds.width-1;
            }
        }

        if(xmove<0) {//movimiento a la izquierda
            int tx = (int) (x + xmove + bounds.x ) / Tile.TILEWIDTH;
            //si el tile donde esta el bound es solido muevete
            if (!collisionWithTile(tx, (int) (y + bounds.y) / Tile.TILEHEIGTH)
                    && !collisionWithTile(tx, (int) (y + bounds.y + bounds.height) / Tile.TILEHEIGTH)) {
                x += xmove;
            }else{
                x=tx*Tile.TILEWIDTH+Tile.TILEWIDTH- bounds.x;
            }
        }
    }
    public void Ymove(){
        if(ymove>0) {//movimiento hacia abajo
            int ty = (int) (y + ymove + bounds.y + bounds.height) / Tile.TILEHEIGTH;
            //si el tile donde esta el bound es solido muevete
            if (!collisionWithTile((int) (x + bounds.x) / Tile.TILEWIDTH,ty)
                    && !collisionWithTile((int) (x + bounds.x+ bounds.width) / Tile.TILEWIDTH,ty)){
                y += ymove;
            }else{
                //arreglar gaps
                y=ty*Tile.TILEHEIGTH- bounds.y- bounds.height-1;
            }
        }

        if(ymove<0) {//movimiento hacia arriba
            int ty = (int) (y + ymove + bounds.y) / Tile.TILEHEIGTH;
            //si el tile donde esta el bound es solido muevete
            if (!collisionWithTile((int) (x + bounds.x) / Tile.TILEWIDTH,ty)
                    && !collisionWithTile((int) (x + bounds.x+ bounds.width) / Tile.TILEWIDTH,ty)){
                y += ymove;
            }else{
                //arreglar gaps
                y=ty*Tile.TILEHEIGTH+ Tile.TILEHEIGTH-bounds.y;
            }
        }
    }
    public boolean collisionWithTile(int x,int y){
        return handler.getWorld().getTile(x,y).isSolid();
    }
}
