package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.creatures;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.Game;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.Entity;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Animation;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Assets;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.inventory.Inventory;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Player extends Creature{
    //variables
    Game game; //por ahora sirve para llamar al keymanager que se usa en game
    Animation animPlayer,animPlayerDown,animPlayerUp,animPlayerLeft,animPlayerRight;
    Animation animPLayerAttackDown,animPLayerAttackUp,animPLayerAttackLeft,animPLayerAttackRight;
    //inventory
    private Inventory inventory;
    //attack timer
    private long lastAttackTimer,attackCoolDown=800,attackTimer=attackCoolDown;

    public Player(Handler handler, float x, float y) {
        super(handler,x, y,Creature.DEFAULT_WIDTH,Creature.DEFAULT_HEIGHT);
        //inicializo los valores de el rectangulo de colision x=13, y=27,width=38,height=37
        bounds.x=24;
        bounds.y=50;
        bounds.width=16;
        bounds.height=15;
        animPlayer =new Animation(300,Assets.player);
        animPlayerDown=new Animation(300,Assets.player_down);
        animPlayerUp=new Animation(300,Assets.player_up);
        animPlayerLeft=new Animation(300,Assets.player_left);
        animPlayerRight=new Animation(300,Assets.player_right);
        animPLayerAttackDown=new Animation(300,Assets.playerAttackDown);
        animPLayerAttackUp=new Animation(300,Assets.playerAttackUp);
        animPLayerAttackLeft=new Animation(300,Assets.playerAttackLeft);
        animPLayerAttackRight=new Animation(300,Assets.playerAttackRight);
        inventory=new Inventory(handler);
    }

    @Override
    public void die() {
        System.out.println("you lose :'D");
    }

    @Override
    public void tick() {
        getInput();
        move();
        handler.getGameCamera().CenterOnEntity(this);
        animPlayer.tick();
        animPlayerUp.tick();
        animPlayerLeft.tick();
        animPlayerDown.tick();
        animPlayerRight.tick();
        animPLayerAttackDown.tick();
        animPLayerAttackRight.tick();
        animPLayerAttackLeft.tick();
        animPLayerAttackUp.tick();
        //attack
        checkAttack();
        //inventoru
        inventory.tick();
    }

    private void checkAttack() {
        //timer
        attackTimer+=System.currentTimeMillis()-lastAttackTimer;
        lastAttackTimer=System.currentTimeMillis();
        if(attackTimer < attackCoolDown)
            return;
        //no mhacer nada mientras el inventario este activo
        if(inventory.isActive())
            return;
        //crea un rectangulo de atackes
        Rectangle cb=getCollisionBounds(0,0);
        Rectangle ar=new Rectangle();
        int arSize=20; //rango de ataque
        ar.width=arSize;
        ar.height=arSize;
        if(handler.getKeyManager().aUp){
            //en x me ubico en el centro del bound
            //menos la mitad del rectangulo de ataque
            //en y me ubico en la esquina de arriba del bound
            ar.x=cb.x+ cb.width/2 - arSize/2;
            ar.y=cb.y-arSize;
        }else if(handler.getKeyManager().aDown){
            ar.x=cb.x+ cb.width/2 - arSize/2;
            ar.y=cb.y+cb.height;
        }else if(handler.getKeyManager().aLeft){
            ar.x=cb.x- arSize;
            ar.y=cb.y+cb.height/2-arSize/2;
        }else if(handler.getKeyManager().aRight){
            ar.x=cb.x+ cb.width;
            ar.y=cb.y+cb.height/2-arSize/2;
        }else{
            return;
        }
        attackTimer=0;
        for(Entity e: handler.getWorld().getEntityManager().getEntities()){
            if(e.equals(this))
                continue;
            if(e.getCollisionBounds(0,0).intersects(ar)){
                e.hurt(1);
                return;
            }
        }

    }

    public void getInput(){
        ymove=0;
        xmove=0;
        //deberia haber codigo aqui al abrir el inventario
        if(inventory.isActive())
            return;
        //los inputs se meten aqui
        if(handler.getKeyManager().up) ymove=-speed;
        if(handler.getKeyManager().down) ymove=speed;
        if(handler.getKeyManager().left) xmove=-speed;
        if(handler.getKeyManager().right) xmove=speed;

    }

    @Override
    public void render(Graphics g) {
        g.drawImage(getCurrentAnimationFrame(),(int)(x- handler.getGameCamera().getxOffset()),
                (int)(y- handler.getGameCamera().getyOffset()),width,height,null); //se resta gameCmaera porque es un valor negativo
        //g.setColor(Color.red);
        //g.fillRect((int)(x+bounds.x-handler.getGameCamera().getxOffset()),
          //      (int)(y+bounds.y-handler.getGameCamera().getyOffset()),
            //    bounds.width, bounds.height);

    }
    public void postRender(Graphics g){
        inventory.render(g);
    }
    public BufferedImage getCurrentAnimationFrame(){
        if(handler.getKeyManager().aUp){
            return animPLayerAttackUp.getCurrentFrame();
        }else if(handler.getKeyManager().aDown){
            return animPLayerAttackDown.getCurrentFrame();
        }else if(handler.getKeyManager().aLeft){
            return animPLayerAttackLeft.getCurrentFrame();
        }else if(handler.getKeyManager().aRight){
            return animPLayerAttackRight.getCurrentFrame();
        }else if(xmove<0){
            return animPlayerLeft.getCurrentFrame();
        }else
        if(xmove>0){
            return animPlayerRight.getCurrentFrame();
        }else
        if(ymove<0){
            return animPlayerUp.getCurrentFrame();
        }else
        if(ymove>0){
            return animPlayerDown.getCurrentFrame();
        }else{
            return animPlayer.getCurrentFrame();
        }

    }
    //getters


    public Inventory getInventory() {
        return inventory;
    }
}
