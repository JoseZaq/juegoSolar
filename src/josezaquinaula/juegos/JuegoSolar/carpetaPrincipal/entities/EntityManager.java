package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.creatures.Player;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

public class EntityManager {
    //manejar todos los entities
    private Handler handler;
    private Player player;
    private ArrayList<Entity> entities;
    private Comparator<Entity> renderSorter= new Comparator<Entity>() {
        @Override
        public int compare(Entity entity, Entity t1) {
            if(entity.getY()+ entity.height<t1.getY()+t1.height) return -1;
            return 1;
        }
    };

    public EntityManager(Handler handler,Player player){
        this.handler=handler;
        this.player=player;
        entities=new ArrayList<>();
        addEntity(player);
    }
    public void render(Graphics g){
        for(Entity e:entities){
            e.render(g);
        }
        player.postRender(g);
    }
    public void tick(){
        //se usa un iterator porque no hay como borrar
        //cosas de un list dentro de un bucle
        Iterator<Entity> it=entities.iterator();
        while(it.hasNext()){
            Entity e=it.next();
            e.tick();
            if(!e.isActive())
                it.remove();
        }
        entities.sort(renderSorter);
    }
    public void addEntity(Entity e){
        entities.add(e);

    }
    //getters and setters

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }

    public void setEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }
}
