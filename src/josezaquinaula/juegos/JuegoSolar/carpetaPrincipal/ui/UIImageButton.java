package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.ui;

import java.awt.*;
import java.awt.image.BufferedImage;

public class UIImageButton extends  UIObject{
    //setea un objeto tipo boton
    private BufferedImage[] images;
    private ClickListener clicker;
    public UIImageButton(float x, float y, int width, int heigth, BufferedImage[] images,ClickListener clicker) {
        super(x, y, width, heigth);
        this.clicker=clicker;
        this.images=images;
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        if(hovering)
            g.drawImage(images[1],(int)x,(int)y,width,heigth,null);
        else
            g.drawImage(images[0],(int)x,(int)y,width,heigth,null);
    }

    @Override
    public void onClick() {
        //
        clicker.onCLick();
    }

}
