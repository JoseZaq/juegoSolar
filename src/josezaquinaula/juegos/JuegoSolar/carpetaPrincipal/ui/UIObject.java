package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.ui;

import java.awt.*;
import java.awt.event.MouseEvent;

public abstract class UIObject {
    //bjeto de ventana (boton,slider,panel,etc)
    protected float x,y;
    protected int width,heigth;
    protected boolean hovering =false;
    protected Rectangle bounds;
    public UIObject(float x,float y,int width,int heigth){
        this.x=x;
        this.y=y;
        this.heigth=heigth;
        this.width=width;
        bounds=new Rectangle((int)x,(int)y,width,heigth);

    }
    public abstract void tick();
    public abstract void render(Graphics g);
    public abstract void onClick();

    public void onMouseMove(MouseEvent e) {
        if(bounds.contains(e.getX(),e.getY()))
            hovering=true;
        else hovering=false;
    }
    public void onMouseReleased(MouseEvent e){
        if(hovering)
            onClick();
    }


    //getters and setters

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeigth() {
        return heigth;
    }

    public void setHeigth(int heigth) {
        this.heigth = heigth;
    }

    public boolean isHovering() {
        return hovering;
    }

    public void setHovering(boolean hovering) {
        this.hovering = hovering;
    }
}
