package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal;

import javax.swing.*;
import java.awt.*;

public class Display extends JFrame {
    //pantalla del juego
    private Canvas canvas;
    private String title;
    private int width,lenght;

    public Display(String title,int width,int lenght){
        //AJUSTAR LA PANTALLA
        this.title=title;
        this.width=width;
        this.lenght=lenght;
        setTitle(title);
        setSize(width,lenght);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);//no redimensionble
        setLocationRelativeTo(null);//centra la pantalla, normalmente se adecua a un parametro dicho(e este caso null)
        setVisible(true);
        canvas=new Canvas(); //canvas es el que dibuja en la pantalla
        canvas.setPreferredSize(new Dimension(width,lenght));
        canvas.setMinimumSize(new Dimension(width,lenght));
        canvas.setMaximumSize(new Dimension(width,lenght));
        canvas.setFocusable(false);
        add(canvas); //añade al Frame(ventana) al canvas(tela)
        pack(); //ajusta el canvas al Frame
    }

    public Canvas getCanvas() {
        return canvas;
    }
}
