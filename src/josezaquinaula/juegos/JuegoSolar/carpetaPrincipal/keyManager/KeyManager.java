package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.keyManager;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener {
    //variables
    public boolean up,down,left,right;
    public boolean aUp,aDown,aLeft,aRight;
    private boolean[] keys,justPressed,cantPress;

    public KeyManager(){
        keys=new boolean[256];
        justPressed=new boolean[256];
        cantPress=new boolean[256];
    }

    public void tick(){
        //inventario (tecla e)
        //recoore todas las teclas
        //cantpress es para que solo un true abra el inventario
        for(int i=0;i <keys.length;i++){
            if(cantPress[i] && !keys[i]){
                cantPress[i]=false;
            }else if(justPressed[i]){
                cantPress[i]=true;
                justPressed[i]=false;
            }if(!cantPress[i] && keys[i]){
                justPressed[i]=true;
            }
        }
        up=keys[KeyEvent.VK_W];
        down=keys[KeyEvent.VK_S];
        left=keys[KeyEvent.VK_A];
        right=keys[KeyEvent.VK_D];


        aUp=keys[KeyEvent.VK_UP];
        aDown=keys[KeyEvent.VK_DOWN];
        aLeft=keys[KeyEvent.VK_LEFT];
        aRight=keys[KeyEvent.VK_RIGHT];
    }
    public boolean keyJustPressed(int keyCode){
        //para botones que abren ventanas
        if(keyCode < 0 || keyCode >= keys.length)
            return false;
        return justPressed[keyCode];
    }
    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        //codigo de la letra pulsada se guarda en un array y es igual a true
        //seguridad
        if(keyEvent.getKeyCode()<0 || keyEvent.getKeyCode() > keys.length)
            return;
        keys[keyEvent.getKeyCode()]=true;
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        //codigo de la letra liberada se guarda en un array y es igual a falseç
        //seguridad
        if(keyEvent.getKeyCode()<0 || keyEvent.getKeyCode() > keys.length)
            return;
        keys[keyEvent.getKeyCode()]=false;
    }
}
