package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.keyManager;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.ui.UIManager;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class MouseManager implements MouseListener, MouseMotionListener {
    //gestor de datos del mouse
    private boolean leftPressed,rightPressed;
    private int mouseX,mouseY;
    public UIManager uiManager;
    public MouseManager(){

    }
    public void setUiManager(UIManager uiManager){
        this.uiManager=uiManager;
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getButton()==MouseEvent.BUTTON1)
            leftPressed=true;
        else if(e.getButton()==MouseEvent.BUTTON3)
            rightPressed=true;
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton()==MouseEvent.BUTTON1)
            leftPressed=false;
        else if(e.getButton()==MouseEvent.BUTTON3)
            rightPressed=false;
        System.out.println(uiManager);
        if(uiManager!=null){
            uiManager.onMouseReleased(e);
        }
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX=e.getX();
        mouseY=e.getY();
        //uimanager
        if(uiManager!= null){
            uiManager.onMouseMoved(e);
        }
    }

    public boolean isLeftPressed() {
        return leftPressed;
    }

    public boolean isRightPressed() {
        return rightPressed;
    }

    public int getMouseX() {
        return mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

}
