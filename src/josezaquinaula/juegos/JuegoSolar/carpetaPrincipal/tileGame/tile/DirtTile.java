package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.tile;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Assets;

import java.awt.image.BufferedImage;

public class DirtTile extends Tile{

    public DirtTile(int id) {
        super(Assets.tierra, id);
    }

}
