package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.tile;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Assets;

import java.awt.image.BufferedImage;

public class RockTile extends Tile{

    public RockTile(int id) {
        super(Assets.roca, id);
    }
    @Override
    public boolean isSolid() {
        return true;
    }
}
