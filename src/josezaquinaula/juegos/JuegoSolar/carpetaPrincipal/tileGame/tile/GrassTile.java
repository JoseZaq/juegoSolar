package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.tile;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Assets;

import java.awt.image.BufferedImage;

public class GrassTile extends Tile{

    public GrassTile(int id) {
        super(Assets.hierba, id);
    }

}
