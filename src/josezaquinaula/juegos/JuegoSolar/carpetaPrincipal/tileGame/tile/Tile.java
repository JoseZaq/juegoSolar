package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.tile;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Tile {
    //TILES STUFF HERE
    public static Tile[] tiles=new Tile[256];
    public static Tile grassTile=new GrassTile(0);
    public static Tile dirtTile=new DirtTile(1);
    public static Tile rockTile=new RockTile(2);


    //variables
    public static final int TILEWIDTH=64 , TILEHEIGTH=64;
    private BufferedImage texture;
    private final int id;
    public Tile(BufferedImage texture,int id){
        this.id=id;
        this.texture=texture;
        tiles[id]=this;
    }
    public void render(Graphics g,int x,int y){
        g.drawImage(texture,x,y,TILEWIDTH,TILEHEIGTH,null);
    }

    public void tick(){

    }

    public boolean isSolid(){
        return false;
    }
    public int getId(){
        return id;
    }
}
