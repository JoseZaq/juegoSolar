package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.States;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.Game;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Assets;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.ui.ClickListener;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.ui.UIImageButton;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.ui.UIManager;

import java.awt.*;

public class MenuState extends State{
    private UIManager uiManager;
    public MenuState(Handler handler){
        super(handler);
        uiManager=new UIManager(handler);
        handler.getGame().getMouseManager().setUiManager(uiManager);
        //add objects
        uiManager.addObject(new UIImageButton(200, 200, 128, 128, Assets.btn_start, new ClickListener() {
            @Override
            public void onCLick() {
                handler.getGame().getMouseManager().setUiManager(null);
                State.setState(handler.getGame().gameState);
            }
        }));
    }
    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics g) {
        uiManager.render(g);


    }
}
