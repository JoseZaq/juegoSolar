package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.States;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.Game;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.EntityManager;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.creatures.Player;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.tile.Tile;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.worlds.World;

import java.awt.*;

public class GameState extends State{
    World world;
    public GameState(Handler handler){
        super(handler);
        //entidades
        world=new World(handler,"res/worlds/world1.txt");
        handler.setWorld(world);
    }
    @Override
    public void tick() {
        world.tick();
        //handler.getGameCamera().move(1,1); para mover la pantalla indepedinetmente del jugador
    }
    @Override
    public void render(Graphics g) {
        world.render(g); //crea los tiles
    }
    //VENTANA DE JUEGO
}
