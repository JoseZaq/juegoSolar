package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.States;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.Game;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;

import java.awt.*;

public abstract class State {
    //DISTINGUE ENTRE VENTANAS
    //variables
    private static State currentstate=null;
    protected Handler handler; //para llevarle a player
    //constructor
    public State(Handler handler){
        this.handler=handler;
    }
    //metodos states
    public static void setState(State state){
        currentstate =state;
    }
    public static State getState(){
        return currentstate;
    }
    //metodos abstractos
    public abstract void tick();
    public abstract void render(Graphics g);
}
