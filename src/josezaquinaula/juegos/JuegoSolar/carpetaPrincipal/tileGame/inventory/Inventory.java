package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.inventory;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Assets;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Text;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.items.Item;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class Inventory {
    /*El inventario se divide en dos: lista y pantalla
    * margen= 40
    * lista=2/3 del total sin incluir el margen = 428
    * pantalla=1/3 sin incuir margen=214*/
    private int invX=10,invY=10,invWidht=(int)(720/1.25),invHeight=(int)(480/1.25);
    private int invListCenterX=invX+(int)((40+214)/1.25),invListCenterY=invY+(int)(40+60*2+10*2);
    private int invImageX=(int)((40+428+(214-40)/2)/1.25),invImageY=40+10,invImageWidht=64,invImageHeight=64;
    private int invCountX=invImageX+32,invCountY=invImageY+88;
    private int invListSpacing=65;

    private int selectedItem =0; //index del invenroryitems para la grafica de inventario
    private Handler handler;
    private boolean active=false;
    private ArrayList<Item> inventoryItems;
    public Inventory(Handler handler){
        this.handler=handler;
        inventoryItems=new ArrayList<Item>();

    }
    public void render(Graphics g){
        if(!active)
            return;
        g.drawImage(Assets.inventarySystem,invX,invY,invWidht,invHeight,null);

        int len=inventoryItems.size();
        if(len==0)
            return;
        for(int i =-2;i<=2;i++){
            //i=-5 porque y hasta 6 porque la grafica del inventairo tiene 10 espacios en la lista
            if(selectedItem +i <0 || selectedItem+i >=len){
                continue;
            }
            if(i==0){
                //agrega los > < y el color amarillo en el cuadro del item seleccionado del inventario
                Text.drawString(g,"> "+inventoryItems.get(selectedItem+i).getName()+" <",invListCenterX,invListCenterY+i*invListSpacing,true,
                        Color.YELLOW,Assets.font28);
            }else {
                Text.drawString(g, inventoryItems.get(selectedItem + i).getName(), invListCenterX, invListCenterY + i * invListSpacing, true,
                        Color.WHITE, Assets.font28);
            }
        }
        Item item=inventoryItems.get(selectedItem);
        g.drawImage(item.getTexture(),invImageX,invImageY,invImageWidht,invImageHeight,null);
        Text.drawString(g,String.valueOf(item.getCount()),invCountX,invCountY,true,Color.WHITE,Assets.font28);
    }
    public void tick(){
        //si el boton e esta presionado activa el inventario
        if(handler.getKeyManager().keyJustPressed(KeyEvent.VK_E))
            active=!active;
        if(!active)
            return;
        //mover el inventario por medio de las key arrows
        if(handler.getKeyManager().keyJustPressed(KeyEvent.VK_UP))
            selectedItem--;
        if(handler.getKeyManager().keyJustPressed(KeyEvent.VK_DOWN))
            selectedItem++;
        if(selectedItem<0)
            selectedItem=inventoryItems.size()-1;
        if(selectedItem>=inventoryItems.size())
            selectedItem=0;

    }
    //inventory section
    public void addItem(Item item){
        //añade cantidad de un item
        for(Item i:inventoryItems){
            if(i.getId() == item.getId()){
                i.setCount(i.getCount()+item.getCount());
                return;
            }
        }
        inventoryItems.add(item);
    }
    //getters and setters

    public boolean isActive() {
        return active;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}
