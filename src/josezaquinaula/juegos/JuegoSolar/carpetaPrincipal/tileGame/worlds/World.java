package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.worlds;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.EntityManager;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.creatures.Player;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.staticCreatures.Tree;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.items.ItemManager;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.tile.Tile;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.utils.Utils;

import java.awt.*;

public class World {
    //variables
    private int[][] tiles;
    private int width,height;
    private int spawnX,spawnY;
    private Handler handler;
    //item
    private ItemManager itemManager;
    //entities
    private EntityManager entityManager;
    public World(Handler handler, String path){
        this.handler = handler;
        entityManager=new EntityManager(handler,new Player(handler,250,250));
        itemManager =new ItemManager(handler);
        loadWorld(path);
        entityManager.getPlayer().setX(spawnX);
        entityManager.getPlayer().setY(spawnY);
        entityManager.addEntity(new Tree(handler,200,200));
        entityManager.addEntity(new Tree(handler,200,400));
        entityManager.addEntity(new Tree(handler,200,600));
        entityManager.addEntity(new Tree(handler,440,600));
    }

    public void render(Graphics g){
        //variables de inicio del for (SIRVE PARA DIBUJAR SOLO DENTRO DE LA CAMARA)
        int xStart=(int)Math.max(0,(handler.getGameCamera().getxOffset()/Tile.TILEWIDTH));
        int xEnd=(int)Math.min(width,((handler.getGameCamera().getxOffset()+ handler.getWidth())/Tile.TILEWIDTH)+1);
        int yStart=(int)Math.max(0,(handler.getGameCamera().getyOffset()/Tile.TILEHEIGTH));
        int yEnd=(int)Math.min(height,(handler.getGameCamera().getyOffset()+ handler.getLenght())/Tile.TILEHEIGTH+1);
        for(int x=xStart;x<xEnd;x++){
            for(int y=yStart;y<yEnd;y++){
                getTile(x,y).render(g,(int)(x*Tile.TILEWIDTH- handler.getGameCamera().getxOffset()),(int)(y*Tile.TILEHEIGTH- handler.getGameCamera().getyOffset()));
            }
        }
        //items
        itemManager.render(g);
        //entities
        entityManager.render(g);
    }
    public void tick(){
        entityManager.tick();
        itemManager.tick();
    }

    public Tile getTile(int x,int y){
        //errores de x y Y
        if(x<0 || x>=width || y<0 || y>=height)
            return Tile.grassTile;
        //obtener el tile de la fila xy columna y
        Tile t=Tile.tiles[tiles[x][y]];
        //errores
        if(t==null){
            return Tile.grassTile;
        }
        return t;
    }

    private void loadWorld(String path){
        String file= Utils.loadFileAsString(path);
        String[] tokens=file.split("\\s+"); // \\s: caracter de espacio, \\s+ :uno o mas espacios en blanco
        width=Utils.parseInt(tokens[0]);
        height=Utils.parseInt(tokens[1]);
        spawnX=Utils.parseInt(tokens[2]);
        spawnY=Utils.parseInt(tokens[3]);
        tiles=new int[width][height];
        for(int x=0;x<width;x++){
            for(int y=0;y<height;y++){
                tiles[x][y]=Utils.parseInt(tokens[(x+y*width)+4]); //0 porque quiero el grassTile //osiblemente sea heigth y no widht

            }
        }
    }
    //getters and setters

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public ItemManager getItemManager() {
        return itemManager;
    }

    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
