package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.Game;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.GameCamera;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.keyManager.KeyManager;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.worlds.World;

public class Handler {
    //variables
    Game game;
    World world;
    public Handler(Game game){
        this.game=game;
    }

    //getters and setters
        //getters de game
        public GameCamera getGameCamera(){
            return game.getGameCamera();
        }
        public KeyManager getKeyManager(){
            return game.getKeyManager();
        }
        public int getWidth(){
            return game.getWidth();
        }
        public int getLenght(){
            return game.getLenght();
        }
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }
}
