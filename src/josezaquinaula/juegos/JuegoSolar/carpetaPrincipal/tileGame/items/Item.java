package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.items;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.Assets;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Item {
    //handler
    public static Item[] items = new Item[256];
    public static Item woodItem =new Item(Assets.wood,"wood",0);
    public static Item rockItem =new Item(Assets.wood,"rock",1);
    //class
    public static final int ITEMWIDTH=32,ITEMHEIGHT=32;
    protected Handler handler;
    protected BufferedImage texture;
    protected String name;
    protected final int id;
    protected int x,y,count;
    protected Rectangle bounds;
    public boolean pickedUp=false;
    public Item(BufferedImage texture,String name,int id){
        this.texture=texture;
        this.id=id;
        this.name=name;
        count=1; //cantidad de ceirto item en el inventario
        items[id]=this;
        bounds=new Rectangle(x,y,ITEMWIDTH,ITEMHEIGHT);
    }
    public void tick(){
        //cuadrado de recoleccion
        if(handler.getWorld().getEntityManager().getPlayer().getCollisionBounds(0f,0f).intersects(bounds)){
            pickedUp=true;
            handler.getWorld().getEntityManager().getPlayer().getInventory().addItem(this);
        }
    }
    public void render(Graphics g){
        //para mostrar en el juego
        if(handler==null)
            return;
        render(g,(int) (x-handler.getGameCamera().getxOffset()),(int)(y-handler.getGameCamera().getyOffset()));
    }
    public void render(Graphics g,int x,int y){
        //para el inventario
        g.drawImage(texture,x,y,ITEMWIDTH,ITEMHEIGHT,null);

    }
    public Item createItem(int count){
        //para testeo
        Item i=new Item(texture,name,id);
        i.setPickedUp(true);
        i.setCount(count);
        return i;
    }
    public Item createItem(int x,int y){
        Item i=new Item(texture,name,id);
        i.setPosition(x,y);
        return i;
    }
    public void setPosition(int x,int y){
        this.x=x;
        this.y=y;
        //posicion del rectangulo
        bounds.x=x;
        bounds.y=y;
    }
    //getters and setters

    public void setPickedUp(boolean pickedUp) {
        this.pickedUp = pickedUp;
    }

    public boolean isPickedUp() {
        return pickedUp;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public BufferedImage getTexture() {
        return texture;
    }

    public void setTexture(BufferedImage texture) {
        this.texture = texture;
    }

    public int getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }
}
