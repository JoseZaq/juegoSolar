package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx;

import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.Game;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.entities.Entity;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.Handler;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.tileGame.tile.Tile;

public class GameCamera {
    private float xOffset,yOffset;
    private Handler handler;
    public GameCamera(Handler handler, float xOffset, float yOffset){
        this.xOffset=xOffset;
        this.yOffset=yOffset;
        this.handler =handler;
    }
    public void move(float xAmount,float yAmount){
        xOffset+=xAmount;
        yOffset+=yAmount;
        checkBlackSpace();
    }
    public void checkBlackSpace(){
        //IMPIDE MOVER LA CAMARA FUERA DE LOS COSTADOS DEL MAPA
        if(xOffset<0){
            xOffset=0;
        }
        if(yOffset<0){
            yOffset=0;
        }
        if(xOffset>(handler.getWorld().getWidth()* Tile.TILEWIDTH-handler.getGame().getWidth())){
            xOffset=handler.getWorld().getWidth()* Tile.TILEWIDTH-handler.getGame().getWidth();
        }
        if(yOffset>handler.getWorld().getHeight()*Tile.TILEHEIGTH-handler.getGame().getLenght()){
            yOffset=handler.getWorld().getHeight()*Tile.TILEHEIGTH-handler.getGame().getLenght();
        }
    }
    public void CenterOnEntity(Entity e){
        xOffset=e.getX()- handler.getWidth()/2+e.getWidth()/2;
        yOffset=e.getY()- handler.getLenght()/2+e.getHeight()/2;
        checkBlackSpace();
    }
    //getters and setters

    public float getxOffset() {
        return xOffset;
    }

    public void setxOffset(float xOffset) {
        this.xOffset = xOffset;
    }

    public float getyOffset() {
        return yOffset;
    }

    public void setyOffset(float yOffset) {
        this.yOffset = yOffset;
    }
}
