package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx;

import java.awt.image.BufferedImage;

public class SpriteSheet {
    //cortador de imagenes por seleccion
    private BufferedImage sheet;
    public SpriteSheet(BufferedImage sheet){
        this.sheet=sheet;
    }

    public BufferedImage crop(int x,int y,int widht, int lenght){
        return sheet.getSubimage(x,y,widht,lenght); //dibuja un rectangulo seleccionador
    }
}
