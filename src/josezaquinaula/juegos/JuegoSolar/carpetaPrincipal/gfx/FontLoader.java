package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class FontLoader {
    //carga fuentes de letra
    public static Font loadFont(String path, float size){
        try {
            return Font.createFont(Font.TRUETYPE_FONT,new File(path)).deriveFont(Font.PLAIN,size);
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
