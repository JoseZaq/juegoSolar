package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Assets {
    //organiza cada imagen de tu spritesheet
    private static int width=64,lenght=64;
    public static Font font28;
    public static BufferedImage hierba,tierra,roca,tree;
    //items
    public static BufferedImage wood;
    public static BufferedImage[] player,player_down,player_up,player_left,player_right;
    public static BufferedImage[] playerAttackUp,playerAttackDown,playerAttackLeft,playerAttackRight;
    public static BufferedImage[] btn_start;
    //inventario
    public static BufferedImage inventarySystem;
    public static void init(){
        font28=FontLoader.loadFont("res/fonts/A.C.M.E. Explosive.ttf",28);
        SpriteSheet sheet=new SpriteSheet(ImageLoader.loadImage("/textures/texturasMinecraft.png"));
        inventarySystem=ImageLoader.loadImage("/textures/inventario.png");
        //player inicializacion
        player_down=new BufferedImage[2];
        player_up=new BufferedImage[2];
        player_left=new BufferedImage[2];
        player_right=new BufferedImage[2];
        player=new BufferedImage[1];
        playerAttackDown=new BufferedImage[4];
        playerAttackUp=new BufferedImage[4];
        playerAttackLeft=new BufferedImage[4];
        playerAttackRight=new BufferedImage[4];
        //btn_start inicializacion
        btn_start=new BufferedImage[2];
        //player seteo
        player_down[0]=sheet.crop(0,lenght,width,lenght);
        player_down[1]=sheet.crop(width,lenght,width,lenght);
        player_up[0]=sheet.crop(2*width,lenght,width,lenght);
        player_up[1]=sheet.crop(3*width,lenght,width,lenght);
        player_left[0]=sheet.crop(0,2*lenght,width,lenght);
        player_left[1]=sheet.crop(width,lenght*2,width,lenght);
        player_right[0]=sheet.crop(width*2,lenght*2,width,lenght);
        player_right[1]=sheet.crop(width*3,lenght*2,width,lenght);
        player[0]=sheet.crop(0,lenght,width,lenght);
        //player attack seteo
        playerAttackDown[0]=sheet.crop(width*4,lenght*2,width,lenght);
        playerAttackDown[1]=sheet.crop(width*5,lenght*2,width,lenght);
        playerAttackDown[2]=sheet.crop(width*6,lenght*2,width,lenght);
        playerAttackDown[3]=sheet.crop(width*7,lenght*2,width,lenght);
        playerAttackUp[0]=sheet.crop(width*4,lenght*3,width,lenght);
        playerAttackUp[1]=sheet.crop(width*5,lenght*3,width,lenght);
        playerAttackUp[2]=sheet.crop(width*6,lenght*3,width,lenght);
        playerAttackUp[3]=sheet.crop(width*7,lenght*3,width,lenght);
        playerAttackLeft[0]=sheet.crop(width*4,0,width,lenght);
        playerAttackLeft[1]=sheet.crop(width*5,0,width,lenght);
        playerAttackLeft[2]=sheet.crop(width*6,0,width,lenght);
        playerAttackLeft[3]=sheet.crop(width*7,0,width,lenght);
        playerAttackRight[0]=sheet.crop(width*4,lenght,width,lenght);
        playerAttackRight[1]=sheet.crop(width*5,lenght,width,lenght);
        playerAttackRight[2]=sheet.crop(width*6,lenght,width,lenght);
        playerAttackRight[3]=sheet.crop(width*7,lenght,width,lenght);

        //seteo general
        hierba=sheet.crop(width,0,width,lenght);
        tierra=sheet.crop(width*2,0,width,lenght);
        roca=sheet.crop(width*3,0,width,lenght);
        tree=sheet.crop(0,lenght*3,width,lenght*2);
        //items
        wood=sheet.crop(width,lenght*4,width,lenght);
        //seteo btn_start
        btn_start[0]=sheet.crop(width,lenght*3,width,lenght);
        btn_start[1]=sheet.crop(width*2,lenght*3,width,lenght);

    }
}
