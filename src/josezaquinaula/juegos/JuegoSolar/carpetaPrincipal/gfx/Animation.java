package josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx;

import java.awt.image.BufferedImage;

public class Animation {
    //crea animaciones mediante frames de la clase assets
    private int speed,index;
    private BufferedImage[] frames;
    private long timer,lastTime;
    public Animation(int speed,BufferedImage[] frames){
        this.frames=frames;
        this.speed=speed;
        index=0;
        timer=0;
        lastTime=System.currentTimeMillis();
    }
    public void tick(){
        timer+=System.currentTimeMillis()-lastTime;
        lastTime=System.currentTimeMillis();
        if(timer>speed){
            timer=0;
            index++;
            if(index>= frames.length){
                index=0;
            }
        }

    }
    public BufferedImage getCurrentFrame(){
        return frames[index];
    }

}
