package entrenamiento.DesarrolloJuego;

import javax.swing.*;
import java.awt.*;

public class Display extends JFrame{
    private Canvas canvas;
    private int width,length;
    private String title;

    public Display(String title,int width,int length){
        this.title=title;
        this.length=length;
        this.width=width;
        //configurar el display
        setTitle(title);
        setSize(width,length);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //configurar el canvas
        canvas=new Canvas();
        canvas.setMaximumSize(new Dimension(width,length));
        canvas.setMinimumSize(new Dimension(width,length));
        canvas.setPreferredSize(new Dimension(width,length));
        //añadir el canvas
        add(canvas);
        pack();

    }
    //getters

    @Override
    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public Canvas getCanvas() {
        return canvas;
    }

}
