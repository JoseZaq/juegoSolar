package entrenamiento.DesarrolloJuego.Graphics;

import java.awt.image.BufferedImage;

public class Assets {
    // width, height
    public static final int w = 64,h = 64;
    //
    private final SpriteSheet mapSheet;
    private final SpriteSheet sheet;
    public static BufferedImage[][] morgue;
    //player
    public static BufferedImage[] chichoDown = new BufferedImage[6];
    public static BufferedImage[] chichoUp = new BufferedImage[6];
    public static BufferedImage[] chichoLeft = new BufferedImage[4];
    public static BufferedImage[] chichoRight = new BufferedImage[4];
    public static BufferedImage player1;
    //contructor
    public Assets() {
        mapSheet = new SpriteSheet("/textures/maps/morgueTiles.png");
        sheet=new SpriteSheet("/textures/players.png");
        morgue = new BufferedImage[20][10]; //ancho: 20*64 largo: 10*64 (1280 640)
        buildAssets(morgue);
        playerAssets();
    }

    private void playerAssets() {
        //VARs
        int widthPL = 64 * 2;
        int heightPL = 64 * 2;
        //CHICHO
        player1=sheet.cortar(0,0,widthPL, heightPL);
        //chichoFord
        chichoDown[0]=sheet.cortar(widthPL,0, widthPL, heightPL);
        chichoDown[1]= sheet.cortar(widthPL *2,0, widthPL, heightPL);
        chichoDown[2]=sheet.cortar(widthPL *3,0, widthPL, heightPL);
        chichoDown[3]= sheet.cortar(widthPL *4,0, widthPL, heightPL);
        chichoDown[4]= sheet.cortar(widthPL *5,0, widthPL, heightPL);
        chichoDown[5]= sheet.cortar(widthPL *6,0, widthPL, heightPL);

        //chichoBack
        chichoUp[0]=sheet.cortar(widthPL, heightPL, widthPL, heightPL);
        chichoUp[1]= sheet.cortar(widthPL *2, heightPL, widthPL, heightPL);
        chichoUp[2]=sheet.cortar(widthPL *3, heightPL, widthPL, heightPL);
        chichoUp[3]= sheet.cortar(widthPL *4, heightPL, widthPL, heightPL);
        chichoUp[4]= sheet.cortar(widthPL *5, heightPL, widthPL, heightPL);
        chichoUp[5]= sheet.cortar(widthPL *6, heightPL, widthPL, heightPL);
        //chichoright
        chichoRight[0] = sheet.cortar(widthPL, heightPL *2, widthPL, heightPL);
        chichoRight[1] = sheet.cortar(widthPL *2, heightPL *2, widthPL, heightPL);
        chichoRight[2] = sheet.cortar(widthPL *3, heightPL *2, widthPL, heightPL);
        chichoRight[3] = sheet.cortar(widthPL *4, heightPL *2, widthPL, heightPL);
        //chicholeft
        chichoLeft[0] = sheet.cortar(widthPL, heightPL *3, widthPL, heightPL);
        chichoLeft[1] = sheet.cortar(widthPL *2, heightPL *3, widthPL, heightPL);
        chichoLeft[2] = sheet.cortar(widthPL *3, heightPL *3, widthPL, heightPL);
        chichoLeft[3] = sheet.cortar(widthPL *4, heightPL *3, widthPL, heightPL);
        //
    }

    /**
     * rellena el buffer con texturas de las imagenes de mapSheet
     * @param assetsMatrix objeto BufferedImage[][] vacio con dimensiones definidas
     */
    private void buildAssets(BufferedImage[][] assetsMatrix) {
        for (int i = 0; i < assetsMatrix.length; i++) {
            for (int j = 0; j < assetsMatrix[i].length; j++) {
                assetsMatrix[i][j] = mapSheet.cortar(i* w,j* h, w, h);
            }
        }
    }
}
