package entrenamiento.DesarrolloJuego.Graphics;

import entrenamiento.DesarrolloJuego.time.Fps;

import java.awt.image.BufferedImage;

public class Animation {
    /*
    secuenciar las imagenes de un buffered array
     */
    //VARs
    private  int FRAMES_PER_MILISECOND = 200;
    private int index=0;
    private BufferedImage[] frames;
    private Fps fps;
    //contructor
    public Animation(BufferedImage[] frames, int FRAMES_PER_MILISECOND) {
        this.frames = frames;
        this.FRAMES_PER_MILISECOND = FRAMES_PER_MILISECOND;
        fps = new Fps();
    }

    public Animation(BufferedImage[] frames) {
        this.frames = frames;
    }

    //functions
    public BufferedImage bufferAnim(){
        //retorna un elemento del array frames
        if(fps.waitForTrue(FRAMES_PER_MILISECOND)){
            index++;
        }
        if(index == frames.length) index =0; //no sobrepasa el limite de elemtnos del array
        return frames[index];
    }
}
