package entrenamiento.DesarrolloJuego.Graphics;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class CargadorDeImagen {
    public static BufferedImage CargarImagen(String path){
        try {
            return ImageIO.read(CargadorDeImagen.class.getResource(path));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }
}
