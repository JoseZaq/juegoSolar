package entrenamiento.DesarrolloJuego.Graphics;

import entrenamiento.DesarrolloJuego.worlds.Tile;
import josezaquinaula.juegos.JuegoSolar.carpetaPrincipal.gfx.ImageLoader;

import java.awt.image.BufferedImage;

public class SpriteSheet {
    //vaiables
    private final String path;
    BufferedImage sheet;
    public SpriteSheet(String path){
        this.path=path;
    }
    //creador de spritessheets
    public BufferedImage cortar(int a, int b, int x, int y){
        sheet= ImageLoader.loadImage(path);
        return sheet.getSubimage(a,b,x,y);
    }
}
