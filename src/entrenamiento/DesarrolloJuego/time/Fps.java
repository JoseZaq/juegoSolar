package entrenamiento.DesarrolloJuego.time;


public class Fps {
    //time
    int fps=60;
    long currentTime =System.nanoTime();
    long lastTime = currentTime;
    double time = 0;
    double programTime = 0;
    public double index = 0;
    //fps ( para cambiar de frame)
    public int rangeIntPerSecond(int FPS,int index,int max) {
        /*Devuelve un entero que se va sumando en cada reiteracion hasta un rango maximo.
        Se obtiene max numeros de index en 1 segundo
        * inputs: FPS: x veces cada iteracion por segundo,index: entero del rango , max: intervalo
        */
        fps=FPS;
        currentTime = System.nanoTime();
        time = currentTime - lastTime;
        programTime += (time / 1000000000) * fps;
        lastTime = currentTime;
        //sumar index para cambiar de frame al chicho
        if(programTime >= 1) {
            index++;
            if(index == max) index=0;
            programTime--;
        }
        return index;
    }
    public boolean waitForMaxIndex(long ms){
        //retorna el time sumado
        currentTime = System.nanoTime();
        index += currentTime-lastTime;
        lastTime=currentTime;
        if(index >= ms*1000000){
            index=0;
            return true;
        }
        return false;
    }
    //wait-> inputs: index output: index
    public  boolean waitForTrue(long ms){
        //retorna true si time es mayor que ms
        currentTime = System.nanoTime();
        time += currentTime-lastTime;
        lastTime=currentTime;
        if(time >= ms*1000000){
            time=0;
            return true;
        }
        return false;
    }
}
