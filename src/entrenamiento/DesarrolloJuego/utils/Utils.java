package entrenamiento.DesarrolloJuego.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Utils {
    public static String loadFileAsString(String path){
        // lee un archivo de texto y retorna un String
        StringBuilder builder=new StringBuilder(); //unidor de lineas de texto
        BufferedReader br= null; //lector
        try {
            br = new BufferedReader(new FileReader(path)); //lector
            String line;
            // lee el archivo con br y lo guarda en line, y une todo el line con el builder
            while((line=br.readLine())!= null) builder.append(line+"\n");
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }
    public static int parseInt(String number){
        //transforma a number un String con try catch para proteccion
        try{
            return Integer.parseInt(number);
        }catch(NumberFormatException e){
            e.printStackTrace();
            return 0;
        }

    }
}
