package entrenamiento.DesarrolloJuego.worlds;

import entrenamiento.DesarrolloJuego.Graphics.Assets;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Tile {
    // un bloque de imagen para contruir un world
    //VARs
    private int id;
    private BufferedImage texture;
    private boolean solid;

    //constructores
    public Tile(int id, BufferedImage texture) {
        this.id = id;
        this.texture = texture;
        solid = true;
    }
    //functions
    //getters and setters
    public boolean isSolid() {
        return solid;
    }
    public void setSolid(boolean solid) {
        this.solid = solid;
    }

    public int getId() {
        return id;
    }

    public BufferedImage getTexture() {
        return texture;
    }

    public void setTexture(BufferedImage texture) {
        this.texture = texture;
    }

}
