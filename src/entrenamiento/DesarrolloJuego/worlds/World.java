package entrenamiento.DesarrolloJuego.worlds;

import entrenamiento.DesarrolloJuego.Juego;
import entrenamiento.DesarrolloJuego.entities.enemies.Plant;
import entrenamiento.DesarrolloJuego.entities.panas.Player;
import entrenamiento.DesarrolloJuego.utils.Utils;

import java.awt.*;

import static entrenamiento.DesarrolloJuego.entities.panas.Player.DEFAULT_HEIGHT_PLAYER;

public class World {
    // contruye un mundo 2d mediante un archivo de texto
    // aloja toda objeto que use el mundo

    //VARs
    private final int DEFAULT_WIDTH = 64, DEFAULT_HEIGTH = 64;
    private int width=DEFAULT_WIDTH,height=DEFAULT_HEIGTH; //widht and height al dibujar los tiles
    private TileManager tileManager;
    private Tile[][] worldDraw; // mundo compuesto de los tiles necesarios
    //juego
    private Juego juego;
    //camera position
    private int camx;
    private int camy;
    private int speed; //controla la velocidad del jugador
    //main player
    Player player;
    //enemies
    Plant plant;
    public World(Juego juego,int x,int y){
        this.juego = juego;
        tileManager = new TileManager();
        worldDraw = makeWorld();
        //inicializar camera position
        camx =x;
        camy=y;
        player =  new Player(juego,this);
        speed = player.getSpeed();
        //enemies
        plant = new Plant(juego,this,64*10,64*2,64,64);
    }

    /**
     * crea una matriz de tiles que coloca cada tile en la posicion adecuada para el mapa
     * @return mapa de tiles
     */
    private Tile[][] makeWorld() {
        int[][] tilesIdMap = loadTilesWorld();
        //contruye el mundo con los tiles
        Tile[][] makeworld = new Tile[tilesIdMap.length][tilesIdMap[0].length];
        for (int i = 0; i < tilesIdMap.length; i++) {
            for (int j = 0; j < tilesIdMap[i].length; j++) {
                if(tileManager.getTileById(tilesIdMap[i][j])== null)
                    makeworld[i][j] = tileManager.getTileById(1); // si el Id tile no existe, llenalo con el tileId =0
                else makeworld[i][j] = tileManager.getTileById(tilesIdMap[i][j]); //obtiene el tile por su id del tilemanager mediante el tilesmap y lo guarda en worlddraw
            }
        }
        return makeworld;
    }

    //functions

    /**
     * crea una matriz de enteros que son ids de los tiles
     * @return matriz de tiles ids
     */
    public int[][] loadTilesWorld(){
        //crea una matriz de tiles del mundo ( como un mapa de bits de un imagen)
        String worldFile=Utils.loadFileAsString("res/worlds/world2.txt");
        String[] tokens = worldFile.split("\\s+"); //parte el archivo por cada espacio que exista
        int width= Utils.parseInt(tokens[0]);
        int height = Utils.parseInt(tokens[1]);
        int[][] worldTiles = new int[width][height];
        for (int i = 0; i < worldTiles.length; i++) {
            for (int j = 0; j < worldTiles[i].length; j++) {
                worldTiles[i][j] = Utils.parseInt(tokens[i+j*width+2]); //i+j*widht+2 es una formula que permite pasar por todos los tokens con un fori tipo matriz
                //OJO: LOS TOKENS VAN : FILA1, FILA2...,(next column) FILA1, FILA2....
            }
        }
        return worldTiles;
    }
    public void render(Graphics g){
        //dibuja el world con worldDraw object
        /*
         * POSICION DEL GRAFICO: POSICION X - XPLAYER
         *                       POSICION Y - YPLAYER
         */
        renderMap(g);
        player.render(g);
        plant.render(g);
    }

    /**
     * dibuja el mapa en la pantalla teniendo en cuenta los limites de esta para dibujar el mapa solo en el cuadro visible
     * @param g pantalla a dibujat
     */
    private void renderMap(Graphics g) {
        //limites del for: son los tiles que estan dentro de la pantalla
        int xMin = Math.abs(camx) /width - 1; // posicion de la camara / ancho de tile = #tiles a la izquierda a borrar
        int xMax = Math.abs(camx)  / width + juego.getPantalla().getWidth() /width + 2; // inicio del for + # tiles en pantalla + 1 tile
        int yMin = Math.abs(camy)/ height -1;
        int yMax = Math.abs(camy) / height +juego.getPantalla().getLength() / height + 2;
        // si los limites se salen del worldDraw[][], entonces corrigelos
        if( xMin < 0)
            xMin = 0;
        else if ( xMax > worldDraw.length)
            xMax = worldDraw.length;
        if ( yMin < 0)
            yMin = 0;
        else if ( yMax > worldDraw[0].length)
            yMax = worldDraw[0].length;
        //haz el for
        for (int i = xMin; i < xMax; i++) {
            for (int j = yMin; j < yMax; j++) {
                //dibuja en cada elemento de la matriz el tile correpsondiente
                //cada elemento tiene una posicion de i,j en la pantalla
                g.drawImage(worldDraw[i][j].getTexture(),i*width+camx,j*height+camy,null);
            }
        }
    }

    public void tick(){
        player.tick();
        plant.tick();
        getPosition(); // actualiza la posicion del drawImage
    }

    /**
     * obtiene la posicion que las teclas definen al player, siempre y cuando no este en un tile solido
     */
    private void getPosition() {
        //actualiza la posicion de la camara, las teclas de movimiento la modifica
        //en cada tecla de movimiento se checka que no haya un tile solid
        //si lo hay, se coloca al player antes del tile
        if (juego.getKeyManager().isDown()) {
            // si no esta en la mitad no muevas la camara
            if( player.getY() >= juego.getPantalla().getLength()/2- DEFAULT_HEIGHT_PLAYER /2) { //si es mayor de la mitad mueve la camara
                //cheka colision
                if (!player.isDownCollision())
                    camy = camy - speed;
                //si la camaa esta en una esquina, habilita el movimiento del player
                if(camy <= -worldDraw[0].length*height + juego.getPantalla().getLength()){
                    if(!player.isDownCollision())
                        player.setY(player.getY()+speed);
                }
            }else {
                if (!player.isDownCollision())
                    player.setY(player.getY() + speed);
            }

        } else if (juego.getKeyManager().isUp()) {
            //
            if( player.getY() <= juego.getPantalla().getLength()/2- DEFAULT_HEIGHT_PLAYER /2){ // si no esta en la mitad no muevas la camara
                //cheka colisiones
                if (!player.isUpCollision())
                camy = camy + speed;
                //
                if ( camy >= 0 ) {//si camera is out, entonces el player se puede mover
                    if (!player.isUpCollision())
                        player.setY(player.getY() - speed);
                }
            }
            else{
                if (!player.isUpCollision())
                    player.setY(player.getY()-speed);
            }
        } else if (juego.getKeyManager().isLeft()) {
            if (player.getX() <= juego.getPantalla().getWidth() / 2 - DEFAULT_WIDTH / 2){ // si no esta en la mitad no muevas la camara
                if (!player.isLeftCollision())
                    camx = camx + speed;
                // se mueve el player al tocar extremo izquierdo del mapa
                if (camx >= 0) {
                    if (!player.isLeftCollision())
                        player.setX(player.getX() - speed);
                }
            }
            else{
                if (!player.isLeftCollision())
                player.setX(player.getX()-speed);
            }
        } else if (juego.getKeyManager().isRight()) {
            //
            if( player.getX() >= juego.getPantalla().getWidth()/2- DEFAULT_WIDTH /2){ // si estoy de la mitad pa la derecha mueve la camara no el jugador
                if (!player.isRightCollision())
                    camx = camx - speed;
                // si estoy en el extremo derecho mueve posicion del player evitando colisiones
                if(camx <= -worldDraw.length*width + juego.getPantalla().getWidth()){
                    if (!player.isRightCollision())
                        player.setX(player.getX()+speed);
                }
            } else{
                if (!player.isRightCollision()) // si cuando la camara no se mueve( por estar en un borde del mapa) y hay colission a la izquierda, el jugador no semovera
                    player.setX(player.getX()+speed);
            }


        }
        FixCameraOut();
    }

    public void FixCameraOut() {
        if ( camy >= 0 ){ //si camera is out, entonces el player se puede mover
            camy = 0;
        }
        if(camy <= -worldDraw[0].length*height + juego.getPantalla().getLength()){
            camy = -worldDraw[0].length*height+ juego.getPantalla().getLength();
        }
        if( camx >= 0 ){
            camx = 0;
        }
        if(camx <= -worldDraw.length*width + juego.getPantalla().getWidth()){
            camx = -worldDraw.length*width+ juego.getPantalla().getWidth();
        }
    }

    /**
     * verifica que el tile donde se esta parado no sea solido
     * @return true si es solido, false si no lo es
     */
    public static boolean checkCollision(World world, int x, int y){
        //si hay tile solid o un enemy
        return world.getTileByMap(x, y).isSolid()
                || (world.plant.getBounds().contains(x,y));
    }

    //getters and setter

    public Player getPlayer() {
        return player;
    }

    public int getCamx() {
        return camx;
    }

    public int getCamy() {
        return camy;
    }

    /**
     * busca el tile que este dentro de las coordenadas otorgadas
     * @param x corrdenada x
     * @param y coordenada y
     * @return tile de la actual posicion
     */
    public Tile getTileByMap(int x, int y){
        return worldDraw[(x-camx)/width][(y-camy)/height];
    }
}
