package entrenamiento.DesarrolloJuego.worlds;

import entrenamiento.DesarrolloJuego.Graphics.Assets;

import java.util.ArrayList;
import java.util.List;

public class TileManager {
    //VARs
    List<Tile> tileList = new ArrayList<>();
    //contructor

    public TileManager() {
        initList();
        //id de los tiles que no son solidos en la morgue:
        //11,12,13,111
        int[] ids= {11,12,13,111};
        defineNoSolidTiles(ids);
    }

    /**
     * define un tile como no solido
     * usa un for de 0 al lenght de ids[], que tiene como elementos los ids de los tiles no solidos
     * @param ids array de los ids de los tiles que no son solidos
     */
    private void defineNoSolidTiles(int[] ids) {
        for (int id : ids) {
            tileList.get(id - 1).setSolid(false);
        }
    }
    //functions
    /**
     * rellena el tilelist con tiles a partir de assets previamente creados y con un id unico desde 1
     * el id se va otorgando por columna de manera descendente en el .png del asset
     */
    private void initList() {
        for (int i = 0; i < Assets.morgue.length; i++) {
            for (int j = 0; j < Assets.morgue[i].length; j++) {
                tileList.add(new Tile(1+j+i* Assets.morgue[i].length, Assets.morgue[i][j])); // id = 1(empiece desde 1)+j + i*(ancho de j)
            }
        }
    }

    //getters
    /**
     * permite conseguir un tile mediante su id
     * @param id id del tile requerido
     * @return tile
     */
    public Tile getTileById(int id){
        //se obtiene el tile por edio de un id
        for (Tile tile : tileList) {
            if (tile.getId() == id) return tile;
        }
        return null;
    }
}
