package entrenamiento.DesarrolloJuego;

import entrenamiento.DesarrolloJuego.Graphics.Assets;
import entrenamiento.DesarrolloJuego.gestorDelTeclado.KeyManager;
import entrenamiento.DesarrolloJuego.worlds.World;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Juego implements Runnable{
    //VARIABLES
    private BufferStrategy bs;
    private Thread hiloJuego;
    private Display pantalla;
    private Graphics g;
    private final int width;
    private final int height;
    private final String title;
    private boolean running=false;
    private Assets assets;
    private KeyManager keyManager;
    private World world;

    public Juego(String title,int width,int height){
        this.title=title;
        this.width=width;
        this.height =height;
    }
    @Override
    public void run() {
        init();
        //configuracion de fps
        int fps=60;
        double timePerTick=1000000000/fps;
        double delta=0;
        long now;
        long lasTime=System.nanoTime();
        int ticks=0;
        long timer=0;
        //WHILE
        while(running){
            now=System.nanoTime();
            delta+=(now-lasTime)/timePerTick;
            timer+=now-lasTime;
            lasTime=now;
            if (delta>=1){
                render();
                tick();
                ticks++;
                delta--;
            }
        }
        stop();
    }

    private void tick() {
        //gestor de teclado
        keyManager.tick();
        world.tick();
    }

    private void stop() {
        //seguridad
        if(!running) return;
        //parar el hilo
        try {
            hiloJuego.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void render() {
        //CONFIGURAR GRAPHICS
            //configuro el bufferstrategy
        bs=pantalla.getCanvas().getBufferStrategy();
        if (bs==null){
            pantalla.getCanvas().createBufferStrategy(3);
            return;
        }
        g=bs.getDrawGraphics();
        g.clearRect(0,0,width, height);
        //DIBUJAR1
        world.render(g);
        //ajusta graphics
        g.dispose();
        bs.show();
    }

    public void start(){
        //seguridad
        if (running) return;
        //iniciar el run y el while
        running=true;
        hiloJuego =new Thread(this); //creo un hilo de esta clase
        hiloJuego.start();

    }

    private void init() {
        pantalla=new Display(title,width, height);
        //imagenes jugadores
        assets = new Assets();
        //gesto de teclado
        keyManager =new KeyManager();
        //init Display
        pantalla.addKeyListener(keyManager);
        //world
        world=new World(this,-60,-20);

    }
    //getters and setters

    public World getWorld() {
        return world;
    }


    public KeyManager getKeyManager() {
        return keyManager;
    }
    public Graphics getG() {
        return g;
    }

    public Display getPantalla() {
        //sacar sus dimensiones para centralizar al player
        return pantalla;
    }
}
