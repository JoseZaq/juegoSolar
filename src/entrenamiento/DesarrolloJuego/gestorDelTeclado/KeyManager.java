package entrenamiento.DesarrolloJuego.gestorDelTeclado;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener {
    //variables
    boolean[] key=new boolean[256];
    private boolean up,down,left,right;
    public void tick(){
        up=key[KeyEvent.VK_W];
        down=key[KeyEvent.VK_S];
        left=key[KeyEvent.VK_A];
        right=key[KeyEvent.VK_D];
    }
    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        key[keyEvent.getKeyCode()]=true;
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        key[keyEvent.getKeyCode()]=false;
    }
    //getters and setters

    public boolean isUp() {
        return up;
    }

    public boolean isDown() {

        return down;
    }

    public boolean isLeft() {
        return left;
    }

    public boolean isRight() {
        return right;
    }
}
