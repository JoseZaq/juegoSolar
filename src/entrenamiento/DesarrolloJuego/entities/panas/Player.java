package entrenamiento.DesarrolloJuego.entities.panas;

import entrenamiento.DesarrolloJuego.Graphics.Animation;
import entrenamiento.DesarrolloJuego.Graphics.Assets;
import entrenamiento.DesarrolloJuego.Juego;
import entrenamiento.DesarrolloJuego.entities.Creature;
import entrenamiento.DesarrolloJuego.time.Fps;
import entrenamiento.DesarrolloJuego.worlds.World;

import java.awt.*;

public class Player extends Creature {
    //variables
    public static int DEFAULT_WIDTH_PLAYER =64*2;
    public static int DEFAULT_HEIGHT_PLAYER =64*2; // si no es estatic no se puede poner en el super
    //animacion
    Animation animUp,animDown,animLeft,animRight;
    //daño
    double damage;
    private final Fps fps = new Fps();
    private boolean beingAttacked;

    //contructor
    public Player(Juego juego, World world){
        super(juego,world,64, 64);
        init();
    }

    private void init() {
        //inicializar animaciones
        setBufferedFrames();
        //posicion central de la camara
        x = juego.getPantalla().getWidth()/2- DEFAULT_WIDTH_PLAYER /2;
        y= juego.getPantalla().getLength()/2- DEFAULT_HEIGHT_PLAYER /2;
        //bounds
        bounds.width = 28;
        bounds.height = 28;
        bounds.x = (x+ DEFAULT_WIDTH_PLAYER /2 - bounds.width/2);
        bounds.y = y+ DEFAULT_HEIGHT_PLAYER -bounds.height-5; //-5 son ajustes
        //daño
        damage = 0;
        beingAttacked = false;
        //otras
        speed = 3;
    }

    private void setBufferedFrames() {
        //crea buffered images para cada animacion
        animUp = new Animation(Assets.chichoUp,150);
        animDown = new Animation(Assets.chichoDown,150);
        animLeft = new Animation(Assets.chichoLeft,150);
        animRight = new Animation(Assets.chichoRight,150);
    }

    //metodos

    @Override
    public void setBoundPosition() {
        //actualizar posicion,animacion
        bounds.x = (x+ DEFAULT_WIDTH_PLAYER /2 - bounds.width/2);
        bounds.y = y+ DEFAULT_HEIGHT_PLAYER -bounds.height-5; //-5 son ajustes
    }

    public void addDamage(double d){
        damage += d;
        health -= d;
        setBeingAttacked(true);
    }
    public void render(Graphics g){
        //dibujar objeto
        if(isBeingAttaked()) {
            if(fps.waitForTrue(1000)) {
                setBeingAttacked(false);
            }
            g.setColor(Color.RED);
            g.fillRect(x, y, DEFAULT_WIDTH_PLAYER, DEFAULT_HEIGHT_PLAYER);
            return;
        }
        if(juego.getKeyManager().isDown())
            g.drawImage(animDown.bufferAnim(), x, y, DEFAULT_WIDTH_PLAYER, DEFAULT_HEIGHT_PLAYER, null);
        else if(juego.getKeyManager().isUp())
            g.drawImage(animUp.bufferAnim(), x, y, DEFAULT_WIDTH_PLAYER, DEFAULT_HEIGHT_PLAYER, null);
        else if(juego.getKeyManager().isLeft())
            g.drawImage(animLeft.bufferAnim(),x,y, DEFAULT_WIDTH_PLAYER, DEFAULT_HEIGHT_PLAYER,null);
        else if(juego.getKeyManager().isRight())
            g.drawImage(animRight.bufferAnim(),x,y, DEFAULT_WIDTH_PLAYER, DEFAULT_HEIGHT_PLAYER,null);
        else g.drawImage(Assets.player1,x,y, DEFAULT_WIDTH_PLAYER, DEFAULT_HEIGHT_PLAYER,null);
        //temp
        g.drawRect(attackZone.x,attackZone.y,attackZone.width,attackZone.height);
        g.setColor(Color.red);
        g.drawRect(bounds.x,bounds.y,bounds.width,bounds.height);
        g.drawString(Double.toString(damage),100,10);
    }

    private boolean isBeingAttaked() {
        return beingAttacked;
    }


    public void tick(){
        super.tick();
    }
    //getters and setters

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSpeed() {
        return speed;
    }

    public void setBeingAttacked(boolean beingAttacked) {
        this.beingAttacked = beingAttacked;
    }

    @Override
    public void move() {

    }

    @Override
    protected void attack() {

    }
}
