package entrenamiento.DesarrolloJuego.entities;

import entrenamiento.DesarrolloJuego.Juego;
import entrenamiento.DesarrolloJuego.worlds.World;

import java.awt.*;

public abstract class Creature extends Entity {
    //VARs
    protected Juego juego;
    protected int speed;
    protected Rectangle attackZone;
    //contructor
    public Creature(Juego juego, World world, int width, int height) {
        super(world,width, height);
        this.juego = juego;
        speed = 3;
        attackZone = new Rectangle(x , y,width * 3,height * 3);
        stablishAttackZone();
    }
    //attackzone

    /**
     * Establece la zona de ataque
     */
    protected void stablishAttackZone() {
        attackZone.x = bounds.x - width/2; // la zona de ataque cubre all el perimetro del player
        attackZone.y = bounds.y - height/2;
        attackZone.width = width  + bounds.width; // ancho = ancho tile + ancho bound + ancho tile. Cubre 3 cuadrados de ataque, derecha, centro e izquierda
        attackZone.height = height + bounds.height; // arriba, centro y abajo
    }

    //functions

    protected abstract void move();
    protected abstract void attack();

    @Override
    public void tick() {
        super.tick();
        // actualizar zona de ataque
        stablishAttackZone();
    }

    // collisions
    public boolean isUpCollision(){
        return World.checkCollision(currentWorld, bounds.x , bounds.y - speed - 1) || World.checkCollision(currentWorld, bounds.x + bounds.width, bounds.y - speed - 1); //speed +a para adelantarse al seguiente movimiento ants de chocar
    }
    public boolean isDownCollision(){
        return World.checkCollision(currentWorld, bounds.x, bounds.y + bounds.height + speed + 1) || World.checkCollision(currentWorld, bounds.x + bounds.width, bounds.y + bounds.height + speed + 1);
    }
    public boolean isRightCollision(){
        return World.checkCollision(currentWorld, bounds.x + bounds.width + speed + 1, bounds.y) || World.checkCollision(currentWorld, bounds.x + bounds.width + speed + 1, bounds.y + bounds.height);
    }
    public boolean isLeftCollision(){
        return World.checkCollision(currentWorld, bounds.x - speed - 1, bounds.y) || World.checkCollision(currentWorld, bounds.x - speed - 1, bounds.y + bounds.height);
    }
    //getters and setters

}
