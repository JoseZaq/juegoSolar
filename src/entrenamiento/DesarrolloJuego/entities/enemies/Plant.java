package entrenamiento.DesarrolloJuego.entities.enemies;

import entrenamiento.DesarrolloJuego.Graphics.Assets;
import entrenamiento.DesarrolloJuego.Juego;
import entrenamiento.DesarrolloJuego.time.Fps;
import entrenamiento.DesarrolloJuego.worlds.World;

import java.awt.*;

public class Plant extends Enemy{
    //vars
    private final Fps fps = new Fps(); // time para el attack time
    public Plant(Juego juego, World world, int x, int y,int width, int height) {
        super(juego, world,x,y, width, height);
    }

    @Override
    protected void move() {

    }

    @Override
    protected void attack() {
          if(fps.waitForMaxIndex(1000))
            currentWorld.getPlayer().addDamage(1); //una planta hace daño 1
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.player1,x,y,null);
        //temp
        g.drawRect(attackZone.x,attackZone.y,attackZone.width,attackZone.height);
        g.drawRect(bounds.x,bounds.y,bounds.width,bounds.height);
    }

    @Override
    public void tick() {
        super.tick();
    }

    @Override
    public void addDamage(double d) {

    }
}
