package entrenamiento.DesarrolloJuego.entities.enemies;

import entrenamiento.DesarrolloJuego.Juego;
import entrenamiento.DesarrolloJuego.entities.Creature;
import entrenamiento.DesarrolloJuego.worlds.World;

public abstract class Enemy extends Creature {
    //VARS
    protected int xmove, ymove; // desplazamiento del punto de inicio al punto final dado por un movimiento del enemy,
                                // x = posicion absoluta en el world, xmove = posicion relativa al jugador
    public Enemy(Juego juego, World world, int x,int y, int width, int height) {
        super(juego, world, width, height);
        xmove = x;
        ymove = y;
        this.y = y;
        this.x = x;
    }
    //attackzone
    protected boolean isPlayerOnAttackZone(){
        return (attackZone.contains(currentWorld.getPlayer().getBounds().x,currentWorld.getPlayer().getBounds().y)
        || attackZone.contains(currentWorld.getPlayer().getBounds().x +currentWorld.getPlayer().getBounds().width
                ,currentWorld.getPlayer().getBounds().y + currentWorld.getPlayer().getBounds().height)); // si esquina derecha superior o esquina izquierda inferior estan dentro de la zona es true
    }
    // position
    public void tickPosition(){
        x = xmove + currentWorld.getCamx(); //
        y = ymove +currentWorld.getCamy();
    }
    //tick and render

    @Override
    public void tick() {
        super.tick();
        tickPosition();
        if(isPlayerOnAttackZone())
            attack();
    }
}
