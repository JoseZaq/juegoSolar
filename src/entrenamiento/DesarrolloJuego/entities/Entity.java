package entrenamiento.DesarrolloJuego.entities;

import entrenamiento.DesarrolloJuego.worlds.World;

import java.awt.*;

public abstract class Entity {
    //VARs
    public final int DEFAULT_WIDTH = 64;
    public final int DEFAULT_HEIGHT = 64;
    public final double DEFAULT_HEALTH = 100; //salud general de una entidad
    protected int x,y;
    protected int height,width;
    protected Rectangle bounds;
    //life
    protected double health;
    //world
    protected World currentWorld;

    //contructor
    public Entity(World world,int width,int height) {
        this.height = height;
        this.width = width;
        bounds = new Rectangle(0,0, width, height);
        //world
        currentWorld = world;
        //life
        health = DEFAULT_HEALTH;
        init();
    }

    private void init() {
        //bound
        bounds.x = (x+DEFAULT_WIDTH/2 - bounds.width/2);
        bounds.y = y+DEFAULT_HEIGHT-bounds.height-5; //-5 son ajustes
    }

    // methods
    public void setBoundPosition(){
        bounds.x = x + width - bounds.width/2;
        bounds.y = y + height;
    }
    //functions abatracts
    public abstract void render(Graphics g);
    public void tick(){
        //actualizar bound
        setBoundPosition();
    }
    public abstract void addDamage(double d);

    //getters and setters
    public boolean isCollision() {
        if(World.checkCollision(currentWorld,bounds.x,bounds.y))
            return true;
        else if(World.checkCollision(currentWorld,bounds.x + bounds.width,bounds.y))
            return true;
        else if(World.checkCollision(currentWorld,bounds.x,bounds.y + bounds.height))
            return true;
        else return World.checkCollision(currentWorld, bounds.x + bounds.width, bounds.y + bounds.height);
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public double getHealth() {
        return health;
    }
}
