package entrenamiento.practicaGeneral;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Hilos extends JFrame implements Runnable, KeyListener {
    //variables
    private  Graphics g;
    private int y=0;
    private boolean tecla=false;
    private JLabel botonAPretadoText;
    //runnable

    public static void main(String[] args) {
        Hilos hilo=new Hilos();
        hilo.start();
    }
    public void start() {
        inicializar();
        run();
    }

    private  void inicializar() {
        setSize(720,480);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        g=getGraphics();
        //teclado
        addKeyListener(this);
        JPanel jPanel = new JPanel();
        jPanel.setAlignmentX(100);
        jPanel.setAlignmentY(200);
        add(jPanel);
        botonAPretadoText=new JLabel();
        jPanel.add(botonAPretadoText);
    }

    @Override
    public void run() {
        long lastTime=System.nanoTime();
        long actualTime;
        //fps
        int fps = 60;
        double timePerFrame=(double)1000000000/ fps;
        int FPS=0;
        while(true){
            actualTime=System.nanoTime();
            double time=actualTime-lastTime;
            if(time>=timePerFrame){
                g.clearRect(0,0,720,480);
                FPS++;
                detectKeyword();
                moveRectangle();
                makeRectangle();
                if(FPS==60){
                    System.out.println("fps: "+FPS);
                    FPS=0;
                }
                lastTime=System.nanoTime();
            }
        }
    }

    private void detectKeyword() {
        if(tecla) botonAPretadoText.setText("Tecla Apretada :D");
        else botonAPretadoText.setText("tecla desapretada D:");
    }

    private void moveRectangle() {
        g.setColor(Color.BLACK);
        //private  JFrame ventana;
        int x = 300;
        g.fillRect(x,y,100,100);
        y++;
    }

    private void makeRectangle() {
        g.setColor(Color.BLUE);
        g.fillRect(0,0,300,200);
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        tecla=true;
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        tecla=false;
    }
}
