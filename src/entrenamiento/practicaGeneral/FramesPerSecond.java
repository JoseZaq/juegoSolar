package entrenamiento.practicaGeneral;

import javax.swing.*;
import java.awt.*;

public class FramesPerSecond {
    //FPS
    public static void main(String[] args) {
        //hacer un programa que corra a x veces por segundo
        //graphics y finestra
        JFrame ventana=new JFrame();
        Canvas canvas=new Canvas();
        Graphics g;
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.setSize(1024,1024);
        ventana.add(canvas);
        g = canvas.getGraphics();
        int x=0,y=0;
        //tiempo
        int contadorDeFrames=0;
        int FPS = 60;
        long ultimoTiempo = System.currentTimeMillis(); //tiempo que empieza el programa (horas que pasaron desde 1970)
        double tiempo;
        while(x<1024 || y<1024){
            //tiempo
            long tiempoActual = System.currentTimeMillis(); //tiempo actual ya corriendo el progrma(desde 1970)
            tiempo =(tiempoActual - ultimoTiempo);      //se resta el tiempo actual menos el tiempo de inicio: 100(1970)-1(1970)=90(desde que corrio el programa)
            double secPerFrame = 1000/(double)FPS;
            //graficos
            if (tiempo >= secPerFrame) {
                contadorDeFrames++;
                x++;y++;
                g.clearRect(0,0,1024,1024);
                g.fillRect(x, y, 500, 500);
                if(contadorDeFrames==60) {
                    System.out.println("FPS: " + 1000/secPerFrame);
                    System.out.println("segundos:"+tiempo);
                    System.out.println();
                    contadorDeFrames=0;
                }
                ultimoTiempo=System.currentTimeMillis();
            }
        }

    }
}
