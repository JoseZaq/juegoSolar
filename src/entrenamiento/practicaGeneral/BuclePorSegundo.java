package entrenamiento.practicaGeneral;

import java.util.Locale;
import java.util.Scanner;

public class BuclePorSegundo {
    //tiempo del juego
    public static void main(String[] args) {
        //conseguir que el programa corra una vez por segundo
        //Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int election = 0;
        double tiempo;
        long lastTime = System.currentTimeMillis();
        while (election != 8) {
            long tiempoActual = System.currentTimeMillis();
            tiempo = tiempoActual - lastTime;
            if (tiempo >= 1000) {
                switch (election) {
                    case 1:
                        System.out.println("_______ARISTÓTELES_______");
                        break;
                    case 2:
                        System.out.println("Escribio sobre logica,etica,politica,metafisica y fisica");
                        break;
                    case 3:
                        System.out.println("Lo que mas destaco de él es que pensaba que el fin del ser humano es " +
                                "realizar su proposito de ser vivo y este era:....");
                        break;
                    case 4:
                        System.out.println("el proposito de un ser vivo puede ser: vegetativo, mecanico o racional (no es" +
                                "toy \nseguro si los nombres de estas categorias eran asi )");
                        break;
                    case 5:
                        System.out.println("cualquier persona que quiera sentirse realizado debera entonces pensar, pero " +
                                "no acaba ahí...");
                        break;
                    case 6:
                        System.out.println("el proposito real es ser feliz, buscar la felicidad, y la felicidad " +
                                "se halla en el bien de si mismo, \ncomo un bucle infinito donde no llegas a un punto si no que siempre" +
                                " vuelves a ti mismo.");
                        break;
                    case 7:
                        System.out.println("FIN :D");
                }
                election++;
                lastTime = System.currentTimeMillis();
            }
        }
    }
}
