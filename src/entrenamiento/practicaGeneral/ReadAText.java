package entrenamiento.practicaGeneral;

import java.io.*;

public class ReadAText {
    //leer y mostrar el contenido de un texto



    //VARs
    static String path = "res/worlds/leer.txt";
    static StringBuilder  builder = new StringBuilder();
    static String line="";
    static BufferedReader br;


    public static void main(String[] args) {
        makeRead();
        String output;
        output= builder.toString();
        System.out.println(output);
    }

    private static void makeRead() {


        {
            try {
                br = new BufferedReader(new FileReader(path)); //el lector
                while(line != null){
                    line = br.readLine(); //lee una una linea del texto y lo coloca en line
                    builder.append(line+"\n");} // coloca un espacio por cada linea y añade line al stringbuilderK
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("error");
            }
        }
    }
}
