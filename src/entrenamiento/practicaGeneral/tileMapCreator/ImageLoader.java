package entrenamiento.practicaGeneral.tileMapCreator;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ImageLoader {
    //permite ingresar una imagen
    public static BufferedImage loadImage(String path){
        //path es la direccion de mi imagen
        try {
            return ImageIO.read(ImageLoader.class.getResource(path)); //lee la imagen
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1); //permite parar el progrma si no se cumple el cargado de imagen
        }
        return null;
    }
}
