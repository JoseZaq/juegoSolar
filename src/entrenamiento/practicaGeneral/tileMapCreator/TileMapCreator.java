package entrenamiento.practicaGeneral.tileMapCreator;

import javax.swing.*;
import javax.swing.plaf.FileChooserUI;
import javax.swing.plaf.basic.BasicFileChooserUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

/**
 * quieres crear un programa que muestre una imagen con sprites donde esten todas las texturas necesarias para crear
 * un mapa.
 * el usuario mirará la imagen e indicara en la consola el codi de la textura a poner en el mapa iniciando desde arriba
 * izquiera hasta abajo derecha.
 * al final el programa creará un documento con los id de los tiles puestos en el orden indicado por el usuario
 */
public class TileMapCreator extends JFrame implements ActionListener {
    //vars
    private JPanel mainPanel;
    private int width = 720;
    private int height = 480;
    private MCKeyListener keyManager;
    // tileImagePanel
    JLabel imageLabel = new JLabel();
    JButton butFile = new JButton("Search Image");
    //contructors
    public TileMapCreator(){
        //display

        setTitle("TileMap Creator");
        setPreferredSize(new Dimension(width,height));
        setMinimumSize(new Dimension(width,height));
        setVisible(true);
        //
        JPanel tileImagePanel = new JPanel();
        Canvas canvas = new Canvas();
        JPanel buttonPanel = new JPanel(new GridLayout(2,2));
        // tile image
        butFile.addActionListener(this);

        /// dimensiones
        tileImagePanel = new JPanel();
        tileImagePanel.setMaximumSize(new Dimension(getWidth()/2-5,2*getHeight()/3));
        tileImagePanel.setMinimumSize(new Dimension(getWidth()/2-5,2*getHeight()/3));
        tileImagePanel.setPreferredSize(new Dimension(getWidth()/2-5,2*getHeight()/3));
        imageLabel.setPreferredSize(new Dimension(getWidth()/2-5,2*getHeight()/3));
        tileImagePanel.add(butFile);
        tileImagePanel.add(imageLabel);
        //canvas
        canvas.setMaximumSize(new Dimension(getWidth()/2-5,2*getHeight()/3));
        canvas.setMinimumSize(new Dimension(getWidth()/2-5,2*getHeight()/3));
        canvas.setPreferredSize(new Dimension(getWidth()/2-5,2*getHeight()/3));
        canvas.setBackground(Color.RED);
        //buttonpanel
        Button b1 = new Button("B1");
        buttonPanel.add(b1);
        //mainPanel
        mainPanel = new JPanel(new FlowLayout(FlowLayout.LEADING,5,5));
        mainPanel.add(canvas);
        mainPanel.add(tileImagePanel);
        mainPanel.add(buttonPanel);
        //
        add(mainPanel);
        keyManager = new MCKeyListener();
        addKeyListener(keyManager);
        pack();
        //actions
        Scanner scanner = new Scanner(System.in);
        int row = scanner.nextInt();
        int column = scanner.nextInt();
        int[][] intMap = new int[row][column];
        ///
        for (int i = 0; i < intMap.length; i++) {
            for (int j = 0; j < intMap[i].length; j++) {
                int input = scanner.nextInt();
                intMap[i][j] = input;
            }
        }
        /// escritura
        crearArchivo(intMap,scanner);
    }

    private void crearArchivo(int[][] intMap,Scanner scanner) {
        System.out.print("Nombre del mapa: ");
        String nameMap = scanner.next() + ".txt";
        Path path = Paths.get(String.format("./%s",nameMap));
        try {
            if(!Files.exists(path))
                Files.createFile(path);
            ///
            StringBuilder fileText = new StringBuilder();
            for (int[] ints : intMap) {
                for (int anInt : ints) {
                    fileText.append(anInt).append(" "); // copia los enteros del array en un solo string
                }
                fileText.append("\n");
            }
            Files.writeString(path, fileText.toString(), StandardOpenOption.TRUNCATE_EXISTING); //escribe el string en un file
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        TileMapCreator app = new TileMapCreator();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //file button
            String ruta = "";
            JFileChooser chooser = new JFileChooser("/home/jose/IdeaProjects/juegoSolar/src/entrenamiento/practicaGeneral/tileMapCreator/fondo_estrellas.png"); //actualizar carpeta de default¡
            int respuesta = chooser.showOpenDialog(this);
            if(respuesta == JFileChooser.APPROVE_OPTION){
                ruta = chooser.getSelectedFile().toString();
                Image imagen = new ImageIcon(ruta).getImage();
                ImageIcon imageIcon = new ImageIcon(imagen.getScaledInstance(imageLabel.getPreferredSize().width,imageLabel.getPreferredSize().height,Image.SCALE_DEFAULT)); //para poner la imagen a escala del label
                imageLabel.setIcon(imageIcon);
        }
    }
}
