package entrenamiento.practicaGeneral.tileMapCreator;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MCKeyListener implements KeyListener {
    //vars
    private boolean left,right,up,down;
    //contructors

    public MCKeyListener() {
        left = false;
        right = false;
        up = false;
        down = false;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_LEFT)
            left = true;
        if(e.getKeyCode() == KeyEvent.VK_RIGHT)
            right = true;
        if(e.getKeyCode() == KeyEvent.VK_UP)
            up= true;
        if(e.getKeyCode() == KeyEvent.VK_DOWN)
            down = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_LEFT)
            left = false;
        if(e.getKeyCode() == KeyEvent.VK_RIGHT)
            right = false;
        if(e.getKeyCode() == KeyEvent.VK_UP)
            up= false;
        if(e.getKeyCode() == KeyEvent.VK_DOWN)
            down = false;
    }

    //getters

    public boolean isLeft() {
        return left;
    }

    public boolean isRight() {
        return right;
    }

    public boolean isUp() {
        return up;
    }

    public boolean isDown() {
        return down;
    }
}
