# JUEGO DE MATA AL CHICHO
## DIAGRAMA DE LLAMADO DE CLASES
```mermaid
graph TD;
Principal --> Juego
Juego --> Pantalla
Juego --> Assets
Juego --> Graphics
Juego --> GestorDelTeclado
Juego --> World
Pantalla --> Canvas
World --> Chicho
World --> TileManager
TileManager --> Tile
TileManager --> Assets
Chicho --> Assets
Chicho --> Animation
```
## CLASES
### Principal
| Objeto |  type |          description         |
|:------:|:-----:|:----------------------------:|
|  juego | Juego | lanza el juego y su pantalla |

| Funcion |  in  |  out |     description    |
|:-------:|:----:|:----:|:------------------:|
|   main  | args | void | reproduce el juego |

### Juego

|   Objeto   |      type      |                               description                               |
|:----------:|:--------------:|:-----------------------------------------------------------------------:|
|     bs     | BufferStrategy | reproduce los objetos animados en el canvas con la estrategia de buffer |
| hiloJuego  | Thread         | crea un hilo para hacer varios metodos a la vez                         |
| pantalla   | Display        | pantalla del juego                                                      |
| width      | int            | ancho de la pantalla                                                    |
| height     | int            | altura de  la pantalla                                                  |
| title      | String         | titulo de la pantalla                                                   |
| running    | boolean        | variable del bucle (while) del juego                                    |
| assets     | Assets         | carga las texturas de todo el juego                                     |
| keyManager | KeyManager     | tiene los metodos para determinar la accion de cada teclado en el juego |
| world      | World          | carga el mapa 1 y sus entidades                                         |

|  Funcion |                  in                 | out  |                                                             description                                                             |
|:--------:|:-----------------------------------:|------|:-----------------------------------------------------------------------------------------------------------------------------------:|
|   Juego  | String title, int width, int height | --   |                                                  carga los detalles de la pantalla                                                  |
| run()    | --                                  | void | * inicializa los fps a 60  * empieza el bucle del juego con running de variable                                                     |
| tick()   | --                                  | void | * actualiza datos de los objetos necesarios * llama al tick de keyManager * llama al tick de world                                  |
| render() | --                                  | void | * grafica en la pantalla * llama a los render de los objetos a dibujar                                                              |
| stop()   | --                                  | void | llama al metodo join de hiliJuego                                                                                                   |
| start()  | --                                  | void | * iguala running a true * llama al metodo start() de hiloJuego                                                                      |
| init()   | --                                  | void | * inicializa los objetos necesarios * inicializa a :  - pantalla - assets - keyManager - world * añade el keyListener a la pantalla |

### World

|     Objeto     |     type    |              description             |
|:--------------:|:-----------:|:------------------------------------:|
|  DEFAULT_WIDTH |  final int  |           ancho de un tile           |
| DEFAULT_HEIGHT | final int   | altura de un tile                    |
| tilesIdMap     | int[][]     | mapa de ids de tiles                 |
| worldDraw      | Tile[][]    | mapa de tiles                        |
| tileManager    | TileManager | bodega de tiles                      |
| juego          | Juego       | da objetos del juego a usar          |
| camx           | int         | posicion -x del mapa en la pantalla  |
| camy           | int         | posicion -y del mapa en la pantalla  |
| speed          | int         | velocidad del jugador                |
| player         | Player      | player central del mapa              |

|      Funcion     |             in            | out      |                                                                        description                                                                       |
|:----------------:|:-------------------------:|----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------:|
|       World      | Juego juego, int x, int y | --       |                  * inicializa:  - tileManager - player - worldDraw - camx - camy  - speed * worldDraw = makeWorld() * camx = x, camy = y                 |
| makeWorld()      | --                        | Tile[][] | * cra una matriz Tile con las posiciones en malla de cada tile en el juego                                                                               |
| loadTilesWorld() | --                        | int[][]  | * crea una matriz int de ids tiles en las posiciones del mapa                                                                                            |
| render()         | --                        | void     | * grafica el mapa * llama al metodo render del player                                                                                                    |
| renderMap()      | Graphics                  |  void    | * dibuja la parte del mapa que se encuentra dentro de la pantalla                                                                                        |
| tick()           | --                        | void     |  * actualiza datos del player y de posicion del mapa                                                                                                     |
| getPosition()    | --                        |  void    | * actualiza la posicion del mapa * mueve el mapa de acuerdo el movimiento del player * impide que la pantalla muestre nada fuera de los limites del mapa |
| FixCameraOut()   | --                        | void     | * permite que el jugador se mueva al estar en los limites de mapa                                                                                        |
| checkColission() | int x, int y              | boolean  | * verifica si el tile donde el player se encuentra es solido                                                                                             |
| getTileByMap()   | int x, int y              | Tile     | * devuelve el tile de la posicion x-y                                                                                                                    |